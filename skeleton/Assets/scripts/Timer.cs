﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class Timer : MonoBehaviour
{
    //unity editor variable
    public float goalTime; // number of seconds the counter will count for (max time on the timer)
    public string Level_To_Load;
    public Text TimeTextDisplay;
    public int Timeer = 0;

    //private variable (used internally in the script)
    private float startTime; // time stamp that our timer is starting at. (time stamp = seconds since game started)
    // Start is called before the first frame update

    void Start()
    {
        //time.time = current time stamp ( time since the game started)
        startTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        
        //Declare a variable to contain our end timestamp to be used for comparisons
        float endTime;
        // the end timestamp is just the start timestamp plus how long the object should live (timer max)
        endTime = startTime + goalTime;
        //check if our current time is greater or equal to our end time
        //if it is perform our action
        if (Time.time >= endTime)
        {
            //action : destroy this object
            SceneManager.LoadScene(Level_To_Load);
        }
        TimeTextDisplay.text = (endTime - Time.time).ToString();
    }
}