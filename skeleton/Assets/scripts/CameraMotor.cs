﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMotor : MonoBehaviour // this code sets the camera to follow the player around maintaining a set distance away.
{
    //variables
    private Transform lookAt;
    private Vector3 startOffset;
    private Vector3 moveVector;

    void Start()    // Start is called before the first frame update
    {
        lookAt = GameObject.FindGameObjectWithTag("Player").transform; //sets the look at position to the game object called player
        startOffset = transform.position - lookAt.position; // tells the camera what the offset
    }

    void Update()    // Update is called once per frame
    {
        moveVector = lookAt.position + startOffset;
        transform.position = lookAt.position + startOffset; //tells the camera to follow the look at position whilst keeping the starting distance away from the look at position
    }
}
