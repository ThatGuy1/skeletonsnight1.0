﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMotor : MonoBehaviour
{
    private CharacterController controller;
    private Vector3 moveVector;
    private float speed = 5.0f; //this is the speed variable (mentioned in controls)

    void Start()    // Start is called before the first frame update
    {
        controller = GetComponent<CharacterController>(); //goes to unity to find the character controller used on this object
    }

    void Update()    // Update is called once per frame
    {
        //----------controls--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        //x - left and right
        moveVector.x = (-Input.GetAxisRaw("Vertical")) * speed; // move around using a/left and d/right

            //y - up and down
                //moveVector.y = Input.GetAxisRaw("Vertical") * speed;
                  moveVector.y = Input.GetAxisRaw("Jump") * speed; //- this sets the movement to space but makes them move up without coming back down
                //moveVector.y = verticalVelocity; //this was the original code that made artifical gravity /anti grav(depending on value) on the object

            //z - forward and backward
                //moveVector.z = speed; // set the character to move along the z-axis based on the speed value
                //moveVector.z = Input.GetAxisRaw("Horizontal") * speed; //this moves right (d/right) and left(a/left) but is in the z axis instead)
                moveVector.z = Input.GetAxisRaw("Horizontal") * speed; // move around using w/up and s/down

        //!Don't do this!
            //moveVector.x = Input.GetAxisRaw("vertical") * speed; (this does NOT work as the v is NOT a capital letter)
            //moveVector.x = Input.GetAxisRaw("Vertical") * speed; (this does NOT work as its the wrong  directional field(should be y NOT x)

        //camera follow me----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            controller.Move(moveVector * Time.deltaTime);//new version 1.1
                // original script - controller.Move((Vector3.forward * speed)* Time.deltaTime); //moves the player controller constantly forward(current camera right) at the speed the pc runs per frame * the speed variable
    }
}
