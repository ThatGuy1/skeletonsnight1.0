﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreSystem : MonoBehaviour
{

    public int Score = 0;
    // score text boxes
    public Text scoreDisplay;
    public Text RscoreDisplay;
    public Text GscoreDisplay;
    public Text PscoreDisplay;
    public Text WscoreDisplay;
  
    //totals
    public int RpointsTotal = 0;
    public int PpointsTotal = 0;
    public int GpointsTotal = 0;
    public int WpointsTotal = 0;

    //score values to be added when colliding with x skull
    public int RedValue = -10;
    public int PurpleValue = -5;
    public int GoldValue = 10;
    public int WhiteValue = 1;

    void OnTriggerEnter(Collider other)
    {
        //Score scoreScript = other.GetComponent<Score>();
        Debug.Log("ScoreSystem Triggered");
            if (other.CompareTag("Red") == true) //checks colour of skull
                {
                    RpointsTotal = RpointsTotal + 1; //adds one to the red point total
                    Score = Score + RedValue; //adds -10 points to the current score
                    RscoreDisplay.text = RpointsTotal.ToString();
                    Destroy(other.gameObject);
                    Debug.Log("ScoreSystem Red Triggered");
                }

            if (other.CompareTag("Purple") == true) //checks colour of skull
                //if (other.CompareTag("Purple") == true) without the ; breaks, if i add the; it works..... WHY!
                {
                    Debug.Log("ScoreSystem Purple Triggered");
                    PpointsTotal = PpointsTotal + 1; //adds one to the purple point total
                    Score = Score + PurpleValue;//adds -5 points to the current score
                    PscoreDisplay.text = PpointsTotal.ToString();
                    Destroy(other.gameObject);
                }

            if (other.CompareTag("Gold") == true) //checks colour of skull
                {
                    GpointsTotal = GpointsTotal + 1; //adds one to the gold point total
                    Score = Score + GoldValue;//adds +10 points to the current score
                    GscoreDisplay.text = GpointsTotal.ToString();
                    Destroy(other.gameObject);
                    Debug.Log("ScoreSystem Gold Triggered");
        }

            if (other.CompareTag("White") == true) //checks colour of skull
                {
                    WpointsTotal = WpointsTotal + 1;//adds one to the white point total
                    Score = Score + WhiteValue;//adds +1 point to the current score
                    WscoreDisplay.text = WpointsTotal.ToString();
                    Destroy(other.gameObject);
                    Debug.Log("ScoreSystem White Triggered");
                }

        scoreDisplay.text = Score.ToString();

        //if(other.CompareTag("Purple") == true);         // testing it worked - the yeee thing stands out as nothing else has that many e's
        //{
        //    Debug.Log("yeeeeeeeeeeeeeeeeeeeeeeeee");
        //}

    }
}
