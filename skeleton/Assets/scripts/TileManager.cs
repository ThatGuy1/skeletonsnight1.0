﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileManager : MonoBehaviour
{
    // public variables
    public GameObject[] tilePrefabs;
    public int amnTilesOnScreen = 100; // amount of spawned items
    public float Start_speed = 1000; // used to start the skull velocity from spawn
    public float SkullGravity = -10; // used to slowly make the skull go down eventually becoming negative and dropping
    public float Current_speed = 0; // used to hold the current velocity of the skull
    public float upForce = 100;
    public float DepthForce = 100;
    public Vector3 spawnPos;

    //private variables
    private List<GameObject> activeTiles; //creates a list of the spawned items
    private Transform playerTransform;
    private int lastPrefabIndex = 0; //used to randomize the selected prefab
    private Vector3 moveVector; // attempting to use this to move the skulls
    //moveVector.x //move forwards/backwards  //moveVector.y //move up/down  //moveVector.z //move left/right


    void Start() // Start is called before the first frame update
    {
        Current_speed = Start_speed + SkullGravity++;
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform; //gets the following player info : (position, rotation, scale) and assigns it a class.
        // be careful not to use find game object*s* with tag as that is different to find game object with tag

        activeTiles = new List<GameObject>();
        for (int i = 0; i < amnTilesOnScreen; i++) // creates a loop to set up the base spawns
        {
            SpawnTile(); //adds a spawned item for a total of the current "amount of tiles on screen" value
        }
    }

    void Update() // Update is called once per frame
    {
        GameObject Skull = GameObject.FindGameObjectWithTag(tilePrefabs[0].tag);
            if (Skull == null)
            {
                SpawnTile();
            }
    }

    private void SpawnTile(int PrefabIndex = -1) //spawns the tiles/skull spawner
    {
        GameObject go; 
        go = Instantiate (tilePrefabs [RandomPrefabIndex()]) as GameObject; 
        go.transform.position = spawnPos;
        go.GetComponent<Rigidbody>().AddForce(Vector3.up * upForce);
        go.GetComponent<Rigidbody>().AddForce(Vector3.right * DepthForce);
        activeTiles.Add(go); //adds new tile/spawned object to the list (required to destroy)
        if (/*SkullZ <= 0 &&*/ go.GetComponent<Rigidbody>().transform.position.y <=-10)
            {
                DeleteTile();
            }
        moveVector.y = Current_speed;
        Current_speed = Current_speed + SkullGravity; // new speed = to current speed minus the gravity
    }

    private void DeleteTile()//deletes the spawned tiles
    {
        Destroy(activeTiles[0]);
        activeTiles.RemoveAt(0);
    }
    
    private int RandomPrefabIndex()//randomizes the prefabs / skulls to make the game more random
    {
        if (tilePrefabs.Length <= 1)
            return 0;

        int randomIndex = lastPrefabIndex;
        while(randomIndex == lastPrefabIndex)
            {
                randomIndex = Random.Range(0, tilePrefabs.Length);
            }
        lastPrefabIndex = randomIndex;
        return randomIndex;
    }
}
