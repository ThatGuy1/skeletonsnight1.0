﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class LevelToLoad : MonoBehaviour
{
    public string Level_To_Load;

    public void OnCollisionEnter(Collision collision)
    {
        Debug.Log("colision detected >:)");
        if (collision.collider.CompareTag("Player") == true)
        {
            SceneManager.LoadScene(Level_To_Load);
            //SceneManager.LoadScene("attempt001");
            //Debug.Log("success ", Level_To_Load, " loaded");
            Debug.Log("it worked");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Level_To_Load got triggered");
        if (other.CompareTag("Player") == true)
        {
            Debug.Log("inside the if statement");
            SceneManager.LoadScene(Level_To_Load);
            //SceneManager.LoadScene("attempt001");
            //Debug.Log("success ", Level_To_Load, " loaded");
            Debug.Log("for some reason Triggering it worked");
        }
    }

    void Start()    // Start is called before the first frame update
    {
        // this works at the start when the level is loaded in. Debug.Log("the skull script has activated");
    }

    void Update()    // Update is called once per frame
    {
        // sends console message - updates every frame - Debug.Log("success it worked - update");
    }
}
