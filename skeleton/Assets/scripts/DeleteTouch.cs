﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteTouch : MonoBehaviour
{
    void Start()    // Start is called before the first frame update
    {
        
    }

    void Update()    // Update is called once per frame
    {
           
    }
    private void OnCollisionEnter(Collision collision)
        {
            GameObject Tag = GameObject.FindGameObjectWithTag("Player");
                if (Tag == null)
                    Destroy(collision.collider.gameObject);
                    // Destroy(GameObject.FindGameObjectWithTag("Purple"));
                    // Destroy(GameObject.FindGameObjectWithTag("Gold"));
                    // Destroy(GameObject.FindGameObjectWithTag("White"));
                    // Destroy(GameObject.FindGameObjectWithTag("Red"));
                    Debug.Log("DeleteTouch collision detected");
        }

    private void OnTriggerEnter(Collider other)
    {
        Destroy(other.gameObject);
        Debug.Log("DeleteTouch Trigger detected");
    }
}
